#!/usr/bin/python
import os
import json
import urllib.request

def lambda_handler(event, context):
    url = "https://api.harvestapp.com/v2/time_entries"
    headers = {
        "User-Agent": "Python Harvest API Sample",
        "Authorization": "Bearer " + os.environ.get("HARVEST_ACCESS_TOKEN"),
        "Harvest-Account-ID": os.environ.get("HARVEST_ACCOUNT_ID")
    }

    request = urllib.request.Request(url=url, headers=headers)
    response = urllib.request.urlopen(request, timeout=5)
    responseBody = response.read().decode("utf-8")
    jsonResponse = json.loads(responseBody)

    totalEntries = jsonResponse["total_entries"]
    Log_List = []

    for i in range(0, totalEntries):
        Log_Record = {}

        Log_Record["name"] = jsonResponse["time_entries"][i]["user"]["name"]
        Log_Record["date"] = jsonResponse["time_entries"][i]["created_at"]
        Log_Record["hours"] = jsonResponse["time_entries"][i]["hours"]
        Log_Record["notes"] = jsonResponse["time_entries"][i]["notes"]
        Log_List.append(Log_Record)

    return {
        "statusCode": 200,
        "body": json.dumps(Log_List, indent=4)
    }
