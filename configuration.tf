# Configure the AWS Provider
provider "aws" {
  # version = "1.40"
  profile = "terraform"
  region  = "ap-southeast-2"

  assume_role {
    role_arn     = "arn:aws:iam::${var.acc_id}:role/${var.management_role}"
    session_name = "Terraform"
  }
}

# https://www.terraform.io/docs/backends/types/s3.html
terraform {
  backend "s3" {
    bucket  = "luke-harvest-bucket"
    key     = "lukeharvest/300559404169/terraform.tfstate"
    region  = "ap-southeast-2"
    encrypt = true
    profile = "terraform"
    acl     = "bucket-owner-full-control"

    role_arn     = "arn:aws:iam::300559404169:role/TechconnectDomainMGMT"
    session_name = "Terraform"
  }
}
